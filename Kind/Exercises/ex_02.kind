// type PairBool
// Contains 2 Booleans inside it
type PairBool { 
  new(fst: Bool, snd: Bool)
}

// Returns the first element of a pair
fst(pair: PairBool): Bool
  case pair {
    new: pair.fst
  }

// Returns the second element of a pair
snd(pair: PairBool): Bool
  open pair
  pair.snd

// Returns true if both elements of a PairBool are identical
eql_elements(a: PairBool): Bool 
  open a
  Bool.eql(a.fst, a.snd)
  
// Returns true if both Pairs are equal 
// eql(PairBool.new(false, false), PairBool.new(true, true)) -> false 
// eql(PairBool.new(true, false), PairBool.new(true, false)) -> true
eql(a: PairBool, b: PairBool): Bool
  open a
  open b
  let bool_eql = (x:Bool y:Bool)
    case x y {
      false false: true
      true  true : true
    } default false
  bool_eql(a.fst b.fst) && bool_eql(a.snd b.snd)

// Returns true if both Pairs are different
// not_eql(PairBool.new(false, false), PairBool.new(true, true)) -> true 
// not_eql(PairBool.new(true, false), PairBool.new(true, false)) -> false
not_eql(a: PairBool, b: PairBool): Bool
  Bool.not(eql(a, b))

// Creates a PairBool from 2 Booleans
create(a: Bool, b: Bool): PairBool
  PairBool.new(a b)

// Inverts the values of a PairBool
// neg(PairBool.new(false, false)) -> PairBool.new(true, true)
// neg(PairBool.new(false, true)) -> PairBool.new(true, false)
neg(pair: PairBool): PairBool
  open pair
  PairBool.new(Bool.not(pair.fst), Bool.not(pair.snd))

// Inverts the 2 elements of a PairBool
swap(pair: PairBool): PairBool 
  open pair
  PairBool.new(pair.snd pair.fst)


ex_02: _
  IO {
    // fst(true, false) == true
    IO.print(Bool.show(fst(PairBool.new(true, false))))
    // snd(true, false) == false
    IO.print(Bool.show(snd(PairBool.new(true, false))))
    // eql(true, false) == false
    IO.print(Bool.show(eql_elements(PairBool.new(true, false))))
    // eql(true, true) ==  true
    IO.print(Bool.show(eql_elements(PairBool.new(true, true))))
    // eql({false, false}, {true, true}) == false
    IO.print(Bool.show(eql(PairBool.new(false, false), PairBool.new(true, true))))
    // eql({false, true}, {true, true}) ==  false
    IO.print(Bool.show(eql(PairBool.new(false, true), PairBool.new(true, true))))
    // eql({true, true}, {true, true}) ==   true
    IO.print(Bool.show(eql(PairBool.new(true, true), PairBool.new(true, true))))
    // true
    IO.print(Bool.show(xor(PairBool.new(false, false), PairBool.new(true, true))))
    // true
    IO.print(Bool.show(xor(PairBool.new(false, true), PairBool.new(true, true))))
    // false
    IO.print(Bool.show(xor(PairBool.new(true, true), PairBool.new(true, true))))
    // {true, false}
    IO.print(PairBool.show(create(true, false)))
    // {true, true}
    IO.print(PairBool.show(neg(create(false, false))))
    // {false true}
    IO.print(PairBool.show(swap(create(true, false))))

  }

PairBool.show(pair: PairBool): String
  open pair
  "{" | Bool.show(pair.fst) | ", " | Bool.show(pair.snd) | "}"
